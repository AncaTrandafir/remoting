package ro.ubb.remoting.client.ui;

import org.xml.sax.SAXException;
import ro.ubb.remoting.common.CrudOperations;
import ro.ubb.remoting.common.model.Client;
import ro.ubb.remoting.common.model.Movie;
import ro.ubb.remoting.common.model.ValidatorException;

import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.Scanner;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

/**
 * Created by anca.
 */
public class ClientConsole {
        private CrudOperations crudOperations;
        private Scanner scanner;

        public ClientConsole(CrudOperations crudOperations) {
            this.crudOperations = crudOperations;
            this.scanner = new Scanner(System.in);
        }

        public void runConsole() throws ValidatorException, IOException, SAXException, ParserConfigurationException, ParseException, SQLException {
            printMenu();

            while (true) {
                String option = scanner.nextLine();
                if (option.equals("x")) {
                    break;
                }
                switch (option) {
                    case "1":
                        consoleAddMovie();
                        break;
                    case "2":
                        consolePrintAllMovies();
                        break;
                    case "3":
                        consoleDeleteMovieById();
                        break;
                    case "4":
                        consoleClearMovies();
                        break;
                    case "5":
                        consoleUpdateMovie();
                        break;
                    case "6":
                        consoleAddClient();
                        break;
                    case "7":
                        consolePrintAllClients();
                        break;
                    case "8":
                        consoleDeleteClientById();
                        break;
                    case "9":
                        consoleClearClients();
                        break;
                    case "10":
                        consoleUpdateClient();
                        break;
//                    case "1":
//                        consoleAddRent();
//                        break;
                    case "11":
                        consolePrintAllRentals();
                        break;
                    case "12":
                        consoleDeleteRentalById();
                        break;
//                    case "12":
//                        consoleClearRents();
//                        break;
                    default:
                        System.out.println("this option is not yet implemented");
                }
                printMenu();
            }
        }


    private void printMenu(){
        System.out.println("1-Add movie\n" +
                "2-Print movies\n" +
                "3-Delete movie by id\n" +
                "4-Clear movies\n" +
                "5-Update movies\n" +
                "6-Add client\n" +
                "7-Print clients\n" +
                "8-Delete client by id\n" +
                "9-Clear clients\n" +
                "10-Update clients\n" +
                "11-?Add rent\n" +
                "10-?Print rents\n" +
                "11-?Delete rent by id\n" +
                "12-?Clear rents\n" +
                "x- Exit");
    }

    private void consoleAddMovie() {
        System.out.println("Introdu valori pt movie: id year title price ");
        String addMovie = scanner.nextLine();
        Future<String> listMovies = crudOperations.addMovie(addMovie);
        try {
            System.out.println(listMovies.get());
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
        }
    }

    private void consoleAddClient() {
        System.out.println("Introdu valori pt client: id prenume nume CNP varsta");
        String addClient = scanner.nextLine();
        Future<String> listClients = crudOperations.addClient(addClient);
        try {
            System.out.println(listClients.get());
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
        }
    }

    private void consoleUpdateMovie()  {
        System.out.println("Noi valori pt movie: id year title price ");
        String newValues = scanner.nextLine();
        String[] movieArray = newValues.split(" ");
        Long id = Long.parseLong(movieArray[0]);
        String year = movieArray[1];
        String title = movieArray[2];
        Double price = Double.parseDouble(movieArray[3]);
        Movie movieUpdate = new Movie(null, null, 0);
        movieUpdate.setTitle(title);
        movieUpdate.setYear(year);
        movieUpdate.setPrice(price);
        movieUpdate.setId(id);
        Future<String> listMovies = crudOperations.updateMovie(newValues);
        try {
            System.out.println(listMovies.get());
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
        }
    }

    private void consoleUpdateClient()  {
        System.out.println("Noi valori pt client: id prenume nume CNP varsta ");
        String newValues = scanner.nextLine();
        String[] clientArray = newValues.split(" ");
        Long id = Long.parseLong(clientArray[0]);
        String firstName = clientArray[1];
        String lastName = clientArray[2];
        String CNP = clientArray[3];
        int age = Integer.parseInt(clientArray[4]);
        Client clientUpdate = new Client(null, null, null, 0);
        clientUpdate.setFirstName(firstName);
        clientUpdate.setLastName(lastName);
        clientUpdate.setCNP(CNP);
        clientUpdate.setAge(age);
        clientUpdate.setId(id);
        Future<String> listMovies = crudOperations.updateClient(newValues);
        try {
            System.out.println(listMovies.get());
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
        }
    }


    private void consoleDeleteMovieById() {
        System.out.println("Enter id:");
        Scanner scanner = new Scanner(System.in);
        try {
            String id = scanner.nextLine();
            crudOperations.deleteMovie(id);
            System.out.println("Movie removed!");
        } catch (Exception ex) {
            System.out.println("Errors:\n" + ex.getMessage());
        }
    }


    private void consoleClearMovies() throws IOException, ParserConfigurationException {
            Future<String> all = crudOperations.deleteMovies();
        }


    private Future<String> consolePrintAllMovies() {
        Future<String> listMovies = crudOperations.printMovies();
        return listMovies;
    }

    private void consoleDeleteClientById() {
        System.out.println("Enter id:");
        Scanner scanner = new Scanner(System.in);
        try {
            String id = scanner.nextLine();
            crudOperations.deleteClient(id);
            System.out.println("Client removed!");
        } catch (Exception ex) {
            System.out.println("Errors:\n" + ex.getMessage());
        }
    }

    private Future<String> consolePrintAllClients() {
        Future<String> listClients = crudOperations.printClients();
        return listClients;
    }

    private void consoleClearClients() throws IOException, ParserConfigurationException {
        Future<String> all = crudOperations.deleteClients();
    }

    private void consoleDeleteRentalById() {
        System.out.println("Enter id:");
        Scanner scanner = new Scanner(System.in);
        try {
            String id = scanner.nextLine();
            crudOperations.deleteRent(id);
            System.out.println("Rental removed!");
        } catch (Exception ex) {
            System.out.println("Errors:\n" + ex.getMessage());
        }
    }

    private Future<String> consolePrintAllRentals() {
        Future<String> listRentals = crudOperations.printRents();
        return listRentals;
    }



      }

