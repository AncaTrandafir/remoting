package ro.ubb.remoting.client.service;

import ro.ubb.remoting.client.tcp.TcpClient;
import ro.ubb.remoting.common.CrudOperations;
import ro.ubb.remoting.common.Message;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;

import static ro.ubb.remoting.common.Message.OK;

/**
 * Created by anca.
 */
public class CrudOperationsClient implements CrudOperations {
    private ExecutorService executorService;
    private TcpClient tcpClient;

    public CrudOperationsClient(ExecutorService executorService, TcpClient tcpClient) {
        this.executorService = executorService;
        this.tcpClient = tcpClient;
    }

    @Override
    public Future<String> addMovie(String title) {
        return executorService.submit(() -> {

            Message request = new Message(CrudOperations.ADD_MOVIE, title);

            Message response = tcpClient.sendAndReceive(request);

            String body = response.getBody();

            return body;
        });

    }

    @Override
    public Future<String> addClient(String client) {
        return executorService.submit(() -> {

            Message request = new Message(CrudOperations.ADD_CLIENT, client);

            Message response = tcpClient.sendAndReceive(request);

            String body = response.getBody();

            return body;
        });

    }

    @Override
    public Future<String> addRent(String rent) {
        return null;
    }

    @Override
    public Future<String> deleteMovie(String id) {
        return executorService.submit(() -> {

            Message request = new Message(CrudOperations.DELETE_MOVIE,id);

            Message response = tcpClient.sendAndReceive(request);

            String body = response.getBody();

            return body;
        });
    }

    @Override
    public Future<String> deleteClient(String id) {
        return executorService.submit(() -> {

            Message request = new Message(CrudOperations.DELETE_CLIENT,id);

            Message response = tcpClient.sendAndReceive(request);

            String body = response.getBody();

            return body;
        });
    }

    @Override
    public Future<String> deleteRent(String id) {
        return executorService.submit(() -> {

            Message request = new Message(CrudOperations.DELETE_RENT,id);

            Message response = tcpClient.sendAndReceive(request);

            String body = response.getBody();

            return body;
        });
    }

    @Override
    public Future<String> printMovies() {
        return executorService.submit(() -> {

            Message request = new Message(CrudOperations.PRINT_MOVIES,OK);

            Message response = tcpClient.sendAndReceive(request);

            String movieList = response.getBody();

            return movieList;
        });

    }

    @Override
    public Future<String> printClients() {
        return executorService.submit(() -> {

            Message request = new Message(CrudOperations.PRINT_CLIENTS,OK);

            Message response = tcpClient.sendAndReceive(request);

            String clientList = response.getBody();

            return clientList;
        });
    }

    @Override
    public Future<String> printRents() {
        return executorService.submit(() -> {

            Message request = new Message(CrudOperations.PRINT_RENTS,OK);

            Message response = tcpClient.sendAndReceive(request);

            String rentalList = response.getBody();

            return rentalList;
        });
    }

    @Override
    public Future<String> deleteMovies() {
        return executorService.submit(() -> {

            Message request = new Message(CrudOperations.DELETE_MOVIES,OK);

            Message response = tcpClient.sendAndReceive(request);

            String delete = response.getBody();

            return delete;
        });
    }

    @Override
    public Future<String> deleteClients() {
        return executorService.submit(() -> {

            Message request = new Message(CrudOperations.DELETE_CLIENTS,OK);

            Message response = tcpClient.sendAndReceive(request);

            String delete = response.getBody();

            return delete;
        });
    }

    @Override
    public Future<String> deleteRents() {
        return executorService.submit(() -> {

            Message request = new Message(CrudOperations.DELETE_RENTS,OK);

            Message response = tcpClient.sendAndReceive(request);

            String delete = response.getBody();

            return delete;
        });
    }

    @Override
    public Future<String> updateMovie(String id) {
        return executorService.submit(() -> {

            Message request = new Message(CrudOperations.UPDATE_MOVIE, id);

            Message response = tcpClient.sendAndReceive(request);

            String movie = response.getBody();

            return movie;
        });

    }

    @Override
    public Future<String> updateClient(String id) {
        return executorService.submit(() -> {

            Message request = new Message(CrudOperations.UPDATE_CLIENT, id);

            Message response = tcpClient.sendAndReceive(request);

            String movie = response.getBody();

            return movie;
        });

    }

}
