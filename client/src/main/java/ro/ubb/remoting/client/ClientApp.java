package ro.ubb.remoting.client;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.xml.sax.SAXException;
import ro.ubb.remoting.client.service.CrudOperationsClient;
import ro.ubb.remoting.client.tcp.TcpClient;
import ro.ubb.remoting.client.ui.ClientConsole;
import ro.ubb.remoting.common.CrudOperations;
import ro.ubb.remoting.common.model.Client;

import javax.xml.parsers.ParserConfigurationException;
import java.io.Console;
import java.io.IOException;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Created by anca.
 */
public class ClientApp {
    public static void main(String[] args) throws SAXException, ParserConfigurationException, SQLException, ParseException, IOException {
//        AnnotationConfigApplicationContext context =
//                new AnnotationConfigApplicationContext(
//                        "server.src.main.java.ro.ubb.remoting.server.config"
//                );

        ExecutorService executorService = Executors.newFixedThreadPool(
                Runtime.getRuntime().availableProcessors()
        );

        TcpClient tcpClient = new TcpClient(executorService);
        CrudOperations crudOperations = new CrudOperationsClient(executorService, tcpClient);
//        ClientConsole console = context.getBean(ClientConsole.class);
//        console.runConsole();
        ClientConsole console = new ClientConsole(crudOperations);
        console.runConsole();

        System.out.println("bye client");
    }
    }

