package ro.ubb.remoting.client.tcp;

import ro.ubb.remoting.common.CrudOperations;
import ro.ubb.remoting.common.CrudOperationsException;
import ro.ubb.remoting.common.Message;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.concurrent.ExecutorService;

/**
 * Created by anca.
 */
public class TcpClient {
    private ExecutorService executorService;

    public TcpClient(ExecutorService executorService) {
        this.executorService = executorService;
    }

    public Message sendAndReceive(Message request) {
//        try {
//                // Create the socket
//                Socket clientSocket = new Socket(CrudOperations.HOST, CrudOperations.PORT);
//                // Create the input & output streams to the server
//                ObjectOutputStream outToServer = new ObjectOutputStream(clientSocket.getOutputStream());
//                ObjectInputStream inFromServer = new ObjectInputStream(clientSocket.getInputStream());
        try (var clientSocket = new Socket(CrudOperations.HOST, CrudOperations.PORT);
             var inFromServer = clientSocket.getInputStream();
             var outToServer = clientSocket.getOutputStream()) {

            System.out.println("sending request: " + request);
            request.writeTo(outToServer);
            System.out.println("request sent");

            Message response = new Message();
            response.readFrom(inFromServer);
            System.out.println("received response: " + response);

            return response;

        } catch (IOException e) {
            e.printStackTrace();
            throw new CrudOperationsException(e);
        }

    }
}
