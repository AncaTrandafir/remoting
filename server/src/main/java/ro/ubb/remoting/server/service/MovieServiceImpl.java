package ro.ubb.remoting.server.service;

import ro.ubb.remoting.common.model.Movie;
import ro.ubb.remoting.common.model.MovieService;
import ro.ubb.remoting.server.repository.MovieJDBCRepository;

import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;

/**
 * Created by anca.
 */
public class MovieServiceImpl  implements MovieService<Long, Movie> {
    private MovieJDBCRepository movieRepository;

    public MovieServiceImpl(MovieJDBCRepository movieRepository) {
        this.movieRepository = movieRepository;
    }

    @Override
    public Iterable<Movie> findAll() throws IOException, ParserConfigurationException {
        return movieRepository.findAll();
    }

    @Override
    public void save(Movie movie) throws Exception {
        movieRepository.save(movie);
    }

    @Override
    public Movie update(Movie movie) {
        movieRepository.update(movie);
        return  movie;
    }

    @Override
    public void delete(Long id) throws IOException, ParserConfigurationException {
        movieRepository.delete(id);
    }
}
