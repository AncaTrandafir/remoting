package ro.ubb.remoting.server.repository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcOperations;
import ro.ubb.remoting.common.model.Client;

import java.util.Optional;

/**
 * Created by anca.
 */
public class ClientJDBCRepository extends MovieRepositoryImpl<Long, Client> {
    @Autowired
    private JdbcOperations jdbcOperations;

    @Override
    public Iterable<Client> findAll() {
        String sql = "select * from clients";
        return jdbcOperations.query(sql, (rs, i) -> {
            Long id = rs.getLong("id");
            String firstName = rs.getString("firstName");
            String lastName = rs.getString("lastName");
            String CNP = rs.getString("CNP");
            int age = rs.getInt("age");
            Client client = new Client (firstName, lastName, CNP, age);
            client.setId(id);
            return client;
        });
    }

    @Override
    public Optional save(Client client) {
        Optional<Client> optional = Optional.ofNullable(client);
        String sql = "insert into clients (id, firstName, lastName, CNP, age) values (?,?,?,?,?)";
        jdbcOperations.update(sql, client.getId(), client.getFirstName(), client.getLastName(), client.getCNP(), client.getAge());
        return  optional;
    }

    @Override
    public Optional update(Client client) {
        Optional<Client> optional = Optional.ofNullable(client);
        String sql = "update client set firstName = ?, lastName = ?, CNP = ?, age = ?where id = ?";
        jdbcOperations.update(sql, client.getFirstName(), client.getLastName(), client.getCNP(),client.getAge());
        return optional;
    }

    @Override
    public Optional delete(Long id) {
        String sql = "delete from clients where id =?";
        jdbcOperations.update(sql, id);
        return Optional.empty();
    }

}
