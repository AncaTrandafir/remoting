package ro.ubb.remoting.server.repository;//package ro.ubb.jdbctemplate.repository.InMemory;

import ro.ubb.remoting.common.model.BaseEntity;
import ro.ubb.remoting.common.model.Validator;
import ro.ubb.remoting.common.model.ValidatorException;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;


/**
 * Created by anca.
 */
public class MovieRepositoryImpl<ID extends Serializable, T extends BaseEntity<ID>> implements MovieRepository<ID, T> {
    private Map<ID, T> entities;
    private Validator<T> validator;


    public MovieRepositoryImpl(Validator<T> validator) {
        this.validator = validator;
        this.entities = new HashMap<>();
    }

    public MovieRepositoryImpl() {
    }


    /**
     * Returns all movies.
     */
    @Override
    public Iterable<T> findAll() {
        Set<T> allEntities = entities.entrySet().stream().map(entry -> entry.getValue()).collect(Collectors.toSet());
        return allEntities;
    }


    @Override
    public Optional<T> save(T entity) throws Exception {
        if (entity == null) {
            throw new IllegalArgumentException("id must not be null");
        }
        validator.validate(entity);
        return Optional.ofNullable(entities.putIfAbsent(entity.getId(), entity));
    }

    /**
     * Removes a movie with the given id.
     * @param id the id.
     * @throws RuntimeException if there's no movie with the given id.
     */


    @Override
    public Optional<T> delete(ID id) {
        return Optional.ofNullable(entities.remove(id));
    }


    /**
     * Adds or updates a movie if it already exists.
     * @param entity the movie to add or update.
     */
    @Override
    public Optional<T> update(T entity) throws ValidatorException {
        if (entity == null) {
            throw new IllegalArgumentException("entity must not be null");
        }
        validator.validate(entity);
        return Optional.ofNullable(entities.computeIfPresent(entity.getId(), (k, v) -> entity));
    }


}


