package ro.ubb.remoting.server;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import ro.ubb.remoting.common.CrudOperations;
import ro.ubb.remoting.common.Message;
import ro.ubb.remoting.server.service.CrudOperationsServiceImpl;
import ro.ubb.remoting.server.tcp.TcpServer;

import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;

/**
 * Created by anca.
 */
public class ServerApp {
    public static void main(String[] args) {

        AnnotationConfigApplicationContext context =
                new AnnotationConfigApplicationContext(
                        "ro.ubb.remoting.server.config"
                );

        ExecutorService executorService = context.getBean(ExecutorService.class);

//      ExecutorService executorService = Executors.newFixedThreadPool(
//            Runtime.getRuntime().availableProcessors()
//      );

//        Validator<Movie> movieValidator = new MovieValidator();
//        Validator<Client> clientValidator = new ClientValidator();
//        Validator<Rent> rentValidator = new RentValidator();
//
//        MovieJDBCRepository movieRepository = new MovieJDBCRepository();
//        ClientJDBCRepository clientRepository = new ClientJDBCRepository();
//        RentJDBCRepository rentRepository = new RentJDBCRepository();
//
//        MovieServiceImpl movieService = new MovieServiceImpl(movieRepository);
//        ClientServiceImpl clientService = new ClientServiceImpl(clientRepository);
//        RentServiceImpl rentService = new RentServiceImpl(rentRepository);

 //   CrudOperations crudOperationsService = new CrudOperationsServiceImpl(executorService, movieService, clientService, rentService);

        CrudOperationsServiceImpl crudOperationsService = context.getBean(CrudOperationsServiceImpl.class);

         TcpServer tcpServer = new TcpServer(executorService, CrudOperations.PORT);


        tcpServer.addHandler(CrudOperations.ADD_MOVIE, request -> {
        Future<String> res = crudOperationsService.addMovie(request.getBody());
        try {
            String result = res.get();
            Message response = new Message(Message.OK, result);
            return response;
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
            return new Message(Message.ERROR, e.getMessage());
        }
    });

        tcpServer.addHandler(CrudOperations.ADD_CLIENT, request -> {
        Future<String> result = crudOperationsService.addClient(request.getBody());
        try {
            String greeting = result.get();
            Message response = new Message(Message.OK, greeting);
            return response;
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
            return new Message(Message.ERROR, e.getMessage());
        }
    });

        tcpServer.addHandler(CrudOperations.DELETE_MOVIE, request -> {
            Future<String> result = null;
            try {
                result = crudOperationsService.deleteMovie(request.getBody());
            } catch (IOException e) {
                e.printStackTrace();
            } catch (ParserConfigurationException e) {
                e.printStackTrace();
            }
            try {
                String greeting = result.get();
                Message response = new Message(Message.OK, greeting);
                return response;
            } catch (InterruptedException | ExecutionException e) {
                e.printStackTrace();
                return new Message(Message.ERROR, e.getMessage());
            }
        });

        tcpServer.addHandler(CrudOperations.DELETE_CLIENT, request -> {
            Future<String> result = null;
            try {
                result = crudOperationsService.deleteClient(request.getBody());
            } catch (IOException e) {
                e.printStackTrace();
            } catch (ParserConfigurationException e) {
                e.printStackTrace();
            }
            try {
                String greeting = result.get();
                Message response = new Message(Message.OK, greeting);
                return response;
            } catch (InterruptedException | ExecutionException e) {
                e.printStackTrace();
                return new Message(Message.ERROR, e.getMessage());
            }
        });

        tcpServer.addHandler(CrudOperations.DELETE_RENT, request -> {
            Future<String> result = null;
            try {
                result = crudOperationsService.deleteClient(request.getBody());
            } catch (IOException e) {
                e.printStackTrace();
            } catch (ParserConfigurationException e) {
                e.printStackTrace();
            }
            try {
                String greeting = result.get();
                Message response = new Message(Message.OK, greeting);
                return response;
            } catch (InterruptedException | ExecutionException e) {
                e.printStackTrace();
                return new Message(Message.ERROR, e.getMessage());
            }
        });

        tcpServer.addHandler(CrudOperations.PRINT_MOVIES, request -> {
            Future<String> result = crudOperationsService.printMovies();
            try {
                String greeting = result.get();
                Message response = new Message(Message.OK, greeting);
                return response;
            } catch (InterruptedException | ExecutionException e) {
                e.printStackTrace();
                return new Message(Message.ERROR, e.getMessage());
            }
        });

        tcpServer.addHandler(CrudOperations.PRINT_CLIENTS, request -> {
            Future<String> result = crudOperationsService.printClients();
            try {
                String greeting = result.get();
                Message response = new Message(Message.OK, greeting);
                return response;
            } catch (InterruptedException | ExecutionException e) {
                e.printStackTrace();
                return new Message(Message.ERROR, e.getMessage());
            }
        });

        tcpServer.addHandler(CrudOperations.PRINT_RENTS, request -> {
            Future<String> result = crudOperationsService.printRents();
            try {
                String greeting = result.get();
                Message response = new Message(Message.OK, greeting);
                return response;
            } catch (InterruptedException | ExecutionException e) {
                e.printStackTrace();
                return new Message(Message.ERROR, e.getMessage());
            }
        });

        tcpServer.addHandler(CrudOperations.DELETE_MOVIE, request -> {
            Future<String> result = null;
            try {
                result = crudOperationsService.deleteMovies();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (ParserConfigurationException e) {
                e.printStackTrace();
            }
            try {
                String greeting = result.get();
                Message response = new Message(Message.OK, greeting);
                return response;
            } catch (InterruptedException | ExecutionException e) {
                e.printStackTrace();
                return new Message(Message.ERROR, e.getMessage());
            }
        });


        tcpServer.startServer();

        System.out.println("bye server");
    }
}


