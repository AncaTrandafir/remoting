package ro.ubb.remoting.server.service;

import ro.ubb.remoting.common.model.MovieService;
import ro.ubb.remoting.common.model.Rent;
import ro.ubb.remoting.server.repository.RentJDBCRepository;

import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;

/**
 * Created by anca.
 */
public class RentServiceImpl implements MovieService<Long, Rent> {
    private RentJDBCRepository rentRepository;

    public RentServiceImpl(RentJDBCRepository rentRepository) {
        this.rentRepository = rentRepository;
    }

    @Override
    public Iterable<Rent> findAll() throws IOException, ParserConfigurationException {
        return rentRepository.findAll();
    }

    @Override
    public void save(Rent rent) throws Exception {
        rentRepository.save(rent);
    }

    @Override
    public Rent update(Rent rent) {
        rentRepository.update(rent);
        return rent;
    }

    @Override
    public void delete(Long id) throws IOException, ParserConfigurationException {
        rentRepository.delete(id);
    }
}
