package ro.ubb.remoting.server.repository;

import org.xml.sax.SAXException;
import ro.ubb.remoting.common.model.BaseEntity;
import ro.ubb.remoting.common.model.ValidatorException;

import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.io.Serializable;
import java.util.Optional;

/**
 * Interface for CRUD operations on a Movie
 *
 * @author anca
 */

public interface MovieRepository<ID extends Serializable, T extends BaseEntity<ID>> {

    /**
     * @return all entities.
     */
    Iterable<T> findAll() throws ParserConfigurationException, IOException, SAXException;


    /**
     * Saves the given entity.
     *
     * @param entity must not be null.
     * @return a {@code Movie} - null if the entity was saved otherwise (e.g. id already exists) returns the entity.
     * @throws IllegalArgumentException if the given entity is null.
     */
    Optional<T> save(T entity) throws Exception;


    /**
     * Updates the given entity.
     *
     * @param entity must not be null.
     * @return a {@code Movie} - null if the entity was updated otherwise (e.g. id does not exist) returns the
     * entity.
     * @throws IllegalArgumentException if the given entity is null.
     */
    Optional<T> update(T entity) throws ValidatorException;


    /**
     * Removes the entity with the given id.
     *
     * @param id must not be null.
     * @throws IllegalArgumentException if the given id is null.
     */
    Optional<T> delete(ID id) throws ParserConfigurationException, IOException, SAXException;


}

