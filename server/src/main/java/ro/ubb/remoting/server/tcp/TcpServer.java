package ro.ubb.remoting.server.tcp;

import ro.ubb.remoting.common.CrudOperations;
import ro.ubb.remoting.common.Message;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.function.UnaryOperator;

/**
 * Created by anca.
 */
public class TcpServer {
    private ExecutorService executorService;
    private int port;
    private Map<String, UnaryOperator<Message>> methodHandlers;

    public TcpServer(ExecutorService executorService, int port) {
        this.executorService = executorService;
        this.port = port;
        this.methodHandlers = new HashMap<>();
    }

    public void addHandler(String methodName, UnaryOperator<Message> handler) {
        methodHandlers.put(methodName, handler);
    }

    public void startServer() {
        try (var serverSocket = new ServerSocket(port)) {
            while (true) {
                Socket clientSocket = serverSocket.accept();
                System.out.println("client connected");
                executorService.submit(new ClientHandler(clientSocket));

            }

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    class ClientHandler implements Runnable {
        private Socket socket;

        public ClientHandler(Socket socket) {
            this.socket = socket;
        }

        @Override
        public void run() {
//            try {
//                // Create the socket
//                Socket serverSocket = new Socket(CrudOperations.HOST, CrudOperations.PORT);
//                // Create the input & output streams to the server
//                ObjectOutputStream outToServer = new ObjectOutputStream(serverSocket.getOutputStream());
//                ObjectInputStream inFromServer = new ObjectInputStream(serverSocket.getInputStream());
            try (var inFromServer = socket.getInputStream();
                 var outToServer = socket.getOutputStream()) {
                System.out.println("processing client");

                //Message request - get message from is
                Message request = new Message();
                request.readFrom(inFromServer);
                System.out.println("received request: " + request);


                //compute response of type Message
                Message response = methodHandlers.get(request.getHeader())
                        .apply(request);
                System.out.println("computed response: " + response);

                //write response to os
                response.writeTo(outToServer);
                System.out.println("response sent to client");

            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}

