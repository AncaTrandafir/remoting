package ro.ubb.remoting.server.service;

import ro.ubb.remoting.common.model.Client;
import ro.ubb.remoting.common.model.MovieService;
import ro.ubb.remoting.server.repository.ClientJDBCRepository;

import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;

/**
 * Created by anca.
 */
public class ClientServiceImpl implements MovieService<Long, Client> {
    private ClientJDBCRepository clientRepository;

    public ClientServiceImpl(ClientJDBCRepository clientRepository) {
        this.clientRepository = clientRepository;
    }

    @Override
    public Iterable<Client> findAll() throws IOException, ParserConfigurationException {
        return clientRepository.findAll();
    }

    @Override
    public void save(Client client) throws Exception {
        clientRepository.save(client);
    }

    @Override
    public Client update(Client client) {
        clientRepository.update(client);
        return client;
    }

    @Override
    public void delete(Long id) throws IOException, ParserConfigurationException {
        clientRepository.delete(id);
    }
}
