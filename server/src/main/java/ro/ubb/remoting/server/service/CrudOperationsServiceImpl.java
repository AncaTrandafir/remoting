package ro.ubb.remoting.server.service;

import ro.ubb.remoting.common.CrudOperations;
import ro.ubb.remoting.common.model.Client;
import ro.ubb.remoting.common.model.Movie;
import ro.ubb.remoting.common.model.Rent;

import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.Arrays;
import java.util.List;
import java.util.Set;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Created by anca.
 */
public class CrudOperationsServiceImpl implements CrudOperations {
    private ExecutorService executorService;
    private MovieServiceImpl movieService;
    private ClientServiceImpl clientService;
    private RentServiceImpl rentService;

    public CrudOperationsServiceImpl(ExecutorService executorService, MovieServiceImpl movieService, ClientServiceImpl clientService, RentServiceImpl rentService) {
        this.executorService = executorService;
        this.movieService = movieService;
        this.clientService = clientService;
        this.rentService = rentService;
    }

    private String convertMovieTostring(Movie movie) {
        return movie.toString();
    }

    private Movie convertStringToMovie(String s) {
        String[] movieArray = s.split(" ");
        Long id = Long.parseLong(movieArray[0]);
        String year = movieArray[1];
        String title = movieArray[2];
        Double price = Double.parseDouble(movieArray[3]);
        Movie movie = new Movie (year, title, price);
        movie.setId(id);
        return movie;
    }

    private String convertClientTostring(Client client) {
        return client.toString();
    }

    private Client convertStringToClient(String s) {
        String[] clientArray = s.split(" ");
        Long id = Long.parseLong(clientArray[0]);
        String firstName = clientArray[1];
        String lastName = clientArray[2];
        String CNP = clientArray[3];
        int age = Integer.parseInt(clientArray[4]);
        Client client = new Client(firstName, lastName, CNP, age);
        client.setId(id);
        return client;
    }


    public String movieAdd(String s) throws IOException, ParserConfigurationException {
        Movie movie = convertStringToMovie(s);
        try {
            movieService.save(movie);
        } catch (Exception e) {
            e.printStackTrace();
        }
        List<Movie> movies = (List<Movie>) movieService.findAll();
        String str=movies.toString();
        return str;
    }

    public String clientAdd(String c) throws IOException, ParserConfigurationException {
        Client client = convertStringToClient(c);
        try {
            clientService.save(client);
        } catch (Exception e) {
            e.printStackTrace();
        }
        List<Client> clients = (List<Client>) clientService.findAll();
        String str=clients.toString();
        return str;
    }

    public String rentalAdd(String c) throws IOException, ParserConfigurationException {
        Client client = convertStringToClient(c);
        try {
            clientService.save(client);
        } catch (Exception e) {
            e.printStackTrace();
        }
        List<Movie> movies = (List<Movie>) movieService.findAll();
        String str=movies.toString();
        return str;
    }

    public String movieUpdate(String m) throws IOException, ParserConfigurationException {
        Movie movie = convertStringToMovie(m);
        Movie movieUpdate = movieService.update(movie);
        List<Movie> movies = (List<Movie>) movieService.findAll();
        String str = movies.toString();
        return str;
    }

    public String clientUpdate(String c) throws IOException, ParserConfigurationException {
        Client client = convertStringToClient(c);
        Client clientUpdate = clientService.update(client);
        List<Client> clients = (List<Client>) clientService.findAll();
        String str = clients.toString();
        return str;
    }


    public String printAllMovies() throws ParserConfigurationException, IOException {
        List<Movie> movies = (List<Movie>) movieService.findAll();
        String s=movies.toString();
        return s;
    }

    private String printAllClients() throws ParserConfigurationException, IOException {
//        System.out.println("All clients: \n");
//        Iterable<Movie> movies = movieService.findAll();
//        movies.forEach(System.out::println);
        List<Client> clients = (List<Client>) clientService.findAll();
        String c = clients.toString();
        return c;
    }

    private String printAllRentals() throws ParserConfigurationException, IOException {
        List<Rent> rentals = (List<Rent>) rentService.findAll();
        String r = rentals.toString();
        return r;
    }

    private String deleteMovieById(String i) throws IOException, ParserConfigurationException {
        Long id = Long.parseLong(i);
        movieService.delete(id);
        List<Movie> movies = (List<Movie>) movieService.findAll();
        String str=movies.toString();
        return str;
    }

    private void deleteClientById(Long id) throws IOException, ParserConfigurationException {
        clientService.delete(id);
        System.out.println("Client removed!");
    }

    private String moviesDelete() throws IOException, ParserConfigurationException {
        Iterable<Movie> all = movieService.findAll();
        for (Movie m: all) {
            movieService.delete(m.getId()); }
        //   List<Movie> movies = (List<Movie>) movieService.findAll();
        String str=all.toString();
        return str;
    }

    private void clientsDelete() {
        try {
            Iterable<Client> all = clientService.findAll();
            for (Client c: all) {
                clientService.delete(c.getId());
            }
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        }
        System.out.println("Clients removed!");
    }

    private void rentsDelete() {
        try {
            Iterable<Rent> all = rentService.findAll();
            for (Rent r: all) {
                rentService.delete(r.getId());
            }
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        }
        System.out.println("Rentals removed!");
    }


    @Override
    public Future<String> addMovie(String movie) {
        return executorService.submit(() -> {
            return "Movie added " + movieAdd(movie);
        });
    }

    @Override
    public Future<String> addClient(String client) {
        return executorService.submit(() -> {
            return "Client added " + clientAdd(client);
        });
    }

    @Override
    public Future<String> addRent(String rent) {
        return executorService.submit(() -> {
            return "Rental added " + rentalAdd(rent);
        });
    }

    @Override
    public Future<String> printMovies() {
        return executorService.submit(() -> {
            return "List of all movies " + printAllMovies();
        });
    }

    @Override
    public Future<String> printClients() {
        return executorService.submit(() -> {
            return "List of all clients " + printAllClients();
        });
    }

    @Override
    public Future<String> printRents() {
        return executorService.submit(() -> {
            return "List of all rentals " + printAllRentals();
        });
    }

    @Override
    public Future<String> deleteMovie(String id) throws IOException, ParserConfigurationException {
        return executorService.submit(() -> {
            return "Movie deleted " + deleteMovieById(id);
        });
    }

    @Override
    public Future<String> deleteClient(String id) throws IOException, ParserConfigurationException {
        Long Id = Long.parseLong(id);
        deleteClientById(Id);
        return executorService.submit(() -> {
            return "Client deleted ";
        });
    }

    @Override
    public Future<String> deleteRent(String id) throws IOException, ParserConfigurationException {
        Long Id = Long.parseLong(id);
        deleteClientById(Id);
        return executorService.submit(() -> {
            return "Client deleted ";
        });
    }

    @Override
    public Future<String> deleteMovies() throws IOException, ParserConfigurationException {
        return executorService.submit(() -> {
            return "All movies deleted " + moviesDelete();
        });
    }

    @Override
    public Future<String> deleteClients() {
        clientsDelete();
        return executorService.submit(() -> {
            return "All clients deleted ";
        });
    }

    @Override
    public Future<String> deleteRents() {
        rentsDelete();
        return executorService.submit(() -> {
            return "All rentals deleted ";
        });
    }


    @Override
    public Future<String> updateMovie(String movie) {
        return executorService.submit(() -> {
            return "Movie updated " + movieUpdate(movie);
        });
    }

    @Override
    public Future<String> updateClient(String client) {
        return executorService.submit(() -> {
            return "Client updated " + clientUpdate(client);
        });
    }


}
