package ro.ubb.remoting.server.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import ro.ubb.remoting.client.ui.ClientConsole;
import ro.ubb.remoting.common.model.Client;
import ro.ubb.remoting.common.model.Rent;
import ro.ubb.remoting.server.repository.ClientJDBCRepository;
import ro.ubb.remoting.server.repository.MovieJDBCRepository;
import ro.ubb.remoting.server.repository.MovieRepositoryImpl;
import ro.ubb.remoting.server.repository.RentJDBCRepository;
import ro.ubb.remoting.server.service.ClientServiceImpl;
import ro.ubb.remoting.server.service.CrudOperationsServiceImpl;
import ro.ubb.remoting.server.service.MovieServiceImpl;
import ro.ubb.remoting.server.service.RentServiceImpl;


import java.util.concurrent.*;

/**
 * Created by anca.
 */

@Configuration
public class AppConfig {
    @Bean
    ClientConsole clientConsole() {
        return new ClientConsole(crudOperations());
    }

    @Bean
    CrudOperationsServiceImpl crudOperations() {
        return new CrudOperationsServiceImpl(fixedThreadPool(), movieService(), clientService(), rentService());
    }

    @Bean
    ExecutorService fixedThreadPool() {
        return Executors.newFixedThreadPool(5); }
//        {
//            @Override
//            public void shutdown() {
//
//            }
//
//            @Override
//            public List<Runnable> shutdownNow() {
//                return null;
//            }
//
//            @Override
//            public boolean isShutdown() {
//                return false;
//            }
//
//            @Override
//            public boolean isTerminated() {
//                return false;
//            }
//
//            @Override
//            public boolean awaitTermination(long timeout, TimeUnit unit) throws InterruptedException {
//                return false;
//            }
//
//            @Override
//            public <T> Future<T> submit(Callable<T> task) {
//                return null;
//            }
//
//            @Override
//            public <T> Future<T> submit(Runnable task, T result) {
//                return null;
//            }
//
//            @Override
//            public Future<?> submit(Runnable task) {
//                return null;
//            }
//
//            @Override
//            public <T> List<Future<T>> invokeAll(Collection<? extends Callable<T>> tasks) throws InterruptedException {
//                return null;
//            }
//
//            @Override
//            public <T> List<Future<T>> invokeAll(Collection<? extends Callable<T>> tasks, long timeout, TimeUnit unit) throws InterruptedException {
//                return null;
//            }
//
//            @Override
//            public <T> T invokeAny(Collection<? extends Callable<T>> tasks) throws InterruptedException, ExecutionException {
//                return null;
//            }
//
//            @Override
//            public <T> T invokeAny(Collection<? extends Callable<T>> tasks, long timeout, TimeUnit unit) throws InterruptedException, ExecutionException, TimeoutException {
//                return null;
//            }
//
//            @Override
//            public void execute(Runnable command) {
//
//            }
//        };
//    }




    @Bean
    MovieServiceImpl movieService() {
        return new MovieServiceImpl( movieRepository());
    }

    @Bean
    ClientServiceImpl clientService() {
        return new ClientServiceImpl((ClientJDBCRepository) clientRepository());
    }

    @Bean
    RentServiceImpl rentService() {
        return new RentServiceImpl((RentJDBCRepository) rentRepository());
    }

    @Bean
    MovieJDBCRepository movieRepository() {
        return new MovieJDBCRepository();
    }

    @Bean
    MovieRepositoryImpl<Long, Client> clientRepository() {
        return new ClientJDBCRepository();
    }

    @Bean
    MovieRepositoryImpl<Long, Rent> rentRepository() {
        return new RentJDBCRepository();
    }
}
