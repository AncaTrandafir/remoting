package ro.ubb.remoting.server.repository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcOperations;
import ro.ubb.remoting.common.model.Movie;

import java.util.Optional;

/**
 * Created by anca.
 */
public class MovieJDBCRepository extends MovieRepositoryImpl<Long, Movie> {
    @Autowired
    private JdbcOperations jdbcOperations;

    @Override
    public Iterable<Movie> findAll() {
        String sql = "select * from movies";
        return jdbcOperations.query(sql, (rs, i) -> {
            Long id = rs.getLong("id");
            String year = rs.getString("year");
            String title = rs.getString("title");
            Double price = rs.getDouble("price");
            Movie movie = new Movie(year, title, price);
            movie.setId(id);
            return movie;
        });
    }

    @Override
    public Optional save(Movie movie) {
        Optional<Movie> optional = Optional.ofNullable(movie);
        String sql = "insert into movies (id, year, title, price) values (?, ?,?,?)";
        jdbcOperations.update(sql, movie.getId(), movie.getYear(), movie.getTitle(), movie.getPrice());
        return optional;
    }

    @Override
    public Optional<Movie> update(Movie movie) {
        Optional<Movie> optional = Optional.ofNullable(movie);
        String sql = "update movies set year = ?, title = ?, price = ? where id = ?";
        jdbcOperations.update(sql, movie.getYear(), movie.getTitle(), movie.getPrice(), movie.getId());
        return optional;
    }

    @Override
    public Optional<Movie> delete(Long id) {
        String sql = "delete from movies where id =?";
        jdbcOperations.update(sql, id);
        return Optional.empty();
    }

}
