package ro.ubb.remoting.server.repository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcOperations;
import ro.ubb.remoting.common.model.Rent;

import java.util.Optional;

/**
 * Created by anca.
 */
public class RentJDBCRepository extends MovieRepositoryImpl<Long, Rent> {
    @Autowired
    private JdbcOperations jdbcOperations;

    @Override
    public Iterable<Rent> findAll() {
        String sql = "select * from rents";
        return jdbcOperations.query(sql, (rs, i) -> {
            Long id = rs.getLong("id");
            Long idMovie = rs.getLong("idMovie");
            Long idClient = rs.getLong("idClient");
            String date = rs.getString("date");
            Rent rental = new Rent (idMovie, idClient, date);
            rental.setId(id);
            return rental;
        });
    }

    @Override
    public Optional save(Rent rent) {
        Optional<Rent> optional = Optional.ofNullable(rent);
        String sql = "insert into rents (idMovie, idClient, date) values (?,?,?)";
        jdbcOperations.update(sql, rent.getIdMovie(), rent.getIdClient(), rent.getDate());
        return optional;
    }

    @Override
    public Optional<Rent> update(Rent rent) {
        Optional<Rent> optional = Optional.ofNullable(rent);
        String sql = "update rents set idMovie = ?, idClient = ?, date = ? where id = ?";
        jdbcOperations.update(sql, rent.getIdMovie(), rent.getIdClient(), rent.getDate(), rent.getId());
        return optional;
    }

    @Override
    public Optional<Rent> delete(Long id) {
        String sql = "delete from rents where id =?";
        jdbcOperations.update(sql, id);
        return Optional.empty();
    }
}
