package ro.ubb.remoting.common.model;

import java.util.Objects;

/**
 * @author anca
 */
public class Movie extends BaseEntity<Long> {
    private String year;
    private String title;
    private double price;

    /**
     *
     * @param year movie release
     * @param title movie
     * @param price
     */

    public Movie(String year, String title, double price) {
        this.year = year;
        this.title = title;
        this.price = price;

    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Movie)) return false;
        Movie movie = (Movie) o;
        return Double.compare(movie.getPrice(), getPrice()) == 0 &&
                Objects.equals(getYear(), movie.getYear()) &&
                Objects.equals(getTitle(), movie.getTitle());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getYear(), getTitle(), getPrice());
    }

    @Override
    public String toString() {
        return "Movie{" +super.toString() +
                "year='" + year + '\'' +
                ", title='" + title + '\'' +
                ", price=" + price +
                "} ";
    }
}
