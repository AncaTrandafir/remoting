package ro.ubb.remoting.common.model;

import java.util.Objects;

public class Client extends BaseEntity<Long> {

    private String firstName, lastName, CNP;
    private int age;


    public Client(String firstName, String lastName, String CNP, int age) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.CNP = CNP;
        this.age = age;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getCNP() {
        return CNP;
    }

    public void setCNP(String CNP) {
        this.CNP = CNP;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Client)) return false;
        Client client = (Client) o;
        return getAge() == client.getAge() &&
                getFirstName().equals(client.getFirstName()) &&
                getLastName().equals(client.getLastName()) &&
                getCNP().equals(client.getCNP());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), getFirstName(), getLastName(), getCNP(), getAge());
    }

    @Override
    public String toString() {
        return "Client{" +
                "firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", CNP='" + CNP + '\'' +
                ", age='" + age + '\'' +
                '}';
    }
}


