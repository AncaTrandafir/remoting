package ro.ubb.remoting.common;

public class CrudOperationsException extends RuntimeException {

    public CrudOperationsException() {
    }

    public CrudOperationsException(String message) {
        super(message);
    }

    public CrudOperationsException(String message, Throwable cause) {
        super(message, cause);
    }

    public CrudOperationsException(Throwable cause) {
        super(cause);
    }

    public CrudOperationsException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }

}