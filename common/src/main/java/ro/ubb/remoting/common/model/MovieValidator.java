package ro.ubb.remoting.common.model;

import java.text.ParseException;
import java.text.SimpleDateFormat;

/**
 * @author anca
 */

public class MovieValidator implements Validator<Movie> {
    @Override
    public void validate(Movie entity) throws ValidatorException {
        SimpleDateFormat format = new SimpleDateFormat("yyyy");
        try {
            format.parse(entity.getYear());
        } catch (ParseException pe) {
            throw new RuntimeException("The year of release is not in a correct format!");
        }

        double price;

        if (entity.getPrice() <= 0) {
            throw new RuntimeException("Price must be a positive number");
        }

    }
}
