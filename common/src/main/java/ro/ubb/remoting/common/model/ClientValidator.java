package ro.ubb.remoting.common.model;

import java.util.ArrayList;
import java.util.List;

public class ClientValidator implements Validator<Client> {
    public void validate(Client client) {

        String CNP = client.getCNP();
        String errors = "";
        if (CNP.length() != 13) {
            errors += "CNP must be 13 digits long.\n";
        }

        List<String> list = new ArrayList<>(); // lista de CNP
        list.add(client.getCNP());
        for (int i = 0; i < list.size(); i++) {
            if (client.getCNP().contains(CNP)) {
                errors += "CNP must be unique";
            }
        }

        if (!errors.equals("")) {
            throw new RuntimeException(errors); }



    }
}
