package ro.ubb.remoting.common.model;

/**
 * @author anca
 *
 */
public interface Validator<T> {
    void validate(T entity) throws ValidatorException;
}
