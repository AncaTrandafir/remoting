package ro.ubb.remoting.common.model;

import java.util.Objects;

public class Rent extends BaseEntity<Long> {

    private long idMovie, idClient;
    private String date;

    public Rent(Long idMovie, Long idClient, String date) {
        super();
        this.idMovie = idMovie;
        this.idClient = idClient;
        this.date = date;
    }

    public long getIdMovie() {
        return idMovie;
    }

    public void setIdMovie(long idMovie) {
        this.idMovie = idMovie;
    }

    public long getIdClient() {
        return idClient;
    }

    public void setIdClient(long idClient) {
        this.idClient = idClient;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Rent)) return false;
        Rent rent = (Rent) o;
        return getIdMovie() == rent.getIdMovie() &&
                getIdClient() == rent.getIdClient() &&
                getDate().equals(rent.getDate());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getIdMovie(), getIdClient(), getDate());
    }

    @Override
    public String toString() {
        return "Rent{" +
                "idMovie=" + idMovie +
                ", idClient=" + idClient +
                ", date='" + date + '\'' +
                '}';
    }
}


