package ro.ubb.remoting.common.model;

import java.text.ParseException;
import java.text.SimpleDateFormat;

public class RentValidator implements Validator<Rent> {
    @Override
    public void validate(Rent entity) throws ValidatorException {
        SimpleDateFormat format = new SimpleDateFormat("dd.mm.yyyy");
        try {
            format.parse(entity.getDate());
        } catch (ParseException pe) {
            throw new RuntimeException("The date is not in correct format -> dd.mm.yyyy!");
        }
    }
}
