package ro.ubb.remoting.common;

import ro.ubb.remoting.common.model.Client;
import ro.ubb.remoting.common.model.Movie;
import ro.ubb.remoting.common.model.Rent;

import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.util.concurrent.Future;

/**
 * Created by anca.
 */
public interface CrudOperations {
    int PORT = 1234;
    String HOST = "localhost";

    String ADD_MOVIE = "addMovie";
    String ADD_CLIENT = "addClient";
    String ADD_RENT = "addRent";

    String DELETE_MOVIE = "deleteMovie";
    String DELETE_CLIENT = "deleteClient";
    String DELETE_RENT = "deleteRent";

    String PRINT_MOVIES = "printMovies";
    String PRINT_CLIENTS = "printClients";
    String PRINT_RENTS = "printRents";

    String DELETE_MOVIES = "deleteMovies";
    String DELETE_CLIENTS = "deleteClients";
    String DELETE_RENTS = "deleteRents";

    String UPDATE_MOVIE = "updateMovie";
    String UPDATE_CLIENT = "updateClient";


    Future<String> addMovie(String movie);
    Future<String> addClient(String client);
    Future<String> addRent(String rent);

    Future<String> deleteMovie(String id) throws IOException, ParserConfigurationException;
    Future<String> deleteClient(String id) throws IOException, ParserConfigurationException;
    Future<String> deleteRent(String id) throws IOException, ParserConfigurationException;

    Future<String> printMovies();
    Future<String> printClients();
    Future<String> printRents();

    Future<String> deleteMovies() throws IOException, ParserConfigurationException;
    Future<String> deleteClients();
    Future<String> deleteRents();

    Future<String> updateMovie(String movie);
    Future<String> updateClient(String client);



}
