package ro.ubb.remoting.common.model;


import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.io.Serializable;

/**
 * Created by anca
 */


public interface MovieService<ID extends Serializable, T extends BaseEntity<ID>>{

    Iterable<T> findAll() throws ParserConfigurationException, IOException;

    void save(T entity) throws Exception;

    T update(T entity) throws ValidatorException;

    void delete(Long id) throws ParserConfigurationException, IOException;


}




